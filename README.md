# Fixed Wings for Fab Labs


Fixed wings requires reflex airfoils at least in the root to be stable on flight. Taper angle is highly recommended as they provide:
  - Largest area to play with the components and control the center of mass.
  - Nice stability (yaw)
  - Larger moment in the tip of the flap

We can manufacture the wing as unrolling its (almost) single curvature surface. This makes it a great candidate to be manufactured in a 2.5 axis machine (likely with a knife as we will need to crease our flat preform. The optimal crease is a V cut operation as the Zund provides).

## Design

Free tools are available to let us estimate range and endurance, design it while analyze it(XFLR5) and design it (MACHUP).

As an example, I am starting to design a fixed wing for Bhutan. I used XFLR5 and Rhino.

<img src="img/phoenix.png" alt="LAttice" width="900" >
<img src="img/profanalasis.png" alt="LAttice" width="900" >
<img src="img/cooldata.png" alt="LAttice" width="900" >
<img src="img/wingdesign.png" alt="LAttice" width="900" >
<img src="img/gif.gif" alt="LAttice" width="300" >

## Zund Iterations

<img src="img/zund1.jpeg" alt="LAttice" width="900" >
<img src="img/zund2.jpeg" alt="LAttice" width="900" >
<img src="img/zund3.jpeg" alt="LAttice" width="900" >
<img src="img/zund4.jpeg" alt="LAttice" width="900" >

Its too heavy, we ordered instead 3mm cardboard that will be the material used for the 2nd iteration.



# Quadcopter

Just trying to make the stiffer and light design with the foam material, not thinking about introducing any payload yet as the mission is undefined. 

Made this triangular beam based structures with two locked ribs. All components are structural, and the minimun weight for a 4 inches blade Ive been able to get is 24 grams. 

<img src="img/drone1.jpeg" alt="LAttice" width="900" >
<img src="img/drone2.jpeg" alt="LAttice" width="900" >
<img src="img/drone3.jpeg" alt="LAttice" width="900" >
<img src="img/drone4.jpeg" alt="LAttice" width="900" >
<img src="img/drone5.jpeg" alt="LAttice" width="900" >


